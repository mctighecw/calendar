# README

This is my second practice project in React.
It is a basic online calendar. I'm planning to add more functionality over time.

## App Information

App Name: calendar (React calendar)

Created: Started in late 2016, continued in early 2017

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/calendar)

## Notes

* Uses 'create-react-app' [Link](https://github.com/facebookincubator/create-react-app)
* Uses 'moment.js' [Link](https://www.npmjs.com/package/moment)
* Uses 'react-big-calendar' [Link](https://www.npmjs.com/package/react-big-calendar)

Please feel free to send me feedback or comments.

Last updated: 2025-02-02
