import React, { Component } from 'react';
import Calendar from './components/Calendar';
import Footer from './components/Footer';
import './styles/main.css';
import './styles/fonts.js';

class App extends Component {
  render() {
    return (
      <div>
        <div className="app-header">
          <h2>React Calendar</h2>
        </div>
        <div className="app-body">
          <Calendar />
        </div>
        <Footer />
      </div>
    );
  }
}

export default App;
