import React, { Component } from 'react';
import moment from 'moment';
import BigCalendar from 'react-big-calendar';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import '../styles/calendar-custom.css';
import formats from '../styles/dates-custom.js';

class Calendar extends Component {
  constructor(props, context) {
    super(props, context);
    BigCalendar.momentLocalizer(moment);
  }

  render() {
    return (
      <div>
        <BigCalendar
          selectable
          defaultDate={new Date ()}
          defaultView='week'
          views={['day', 'week', 'month']}
          formats={formats}
          events={[]}
        />
      </div>
    );
  }

};

export default Calendar;
