import React, { Component } from 'react';
import '../styles/calendar-custom.css';

class Footer extends Component {
  render() {
    return (
      <div>
        <footer>
          <h2>&copy; 2017, Christian McTighe. Coded by Hand.</h2>
        </footer>
      </div>
    );
  }

};

export default Footer;
