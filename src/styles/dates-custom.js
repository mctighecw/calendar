// Custom date formats

let formats = {
  dateFormat: 'DD',
  dayFormat: 'ddd, DD',
  weekdayFormat: 'dddd',
  dayHeaderFormat: 'dddd, MMMM DD',
  monthHeaderFormat: 'MMMM YYYY',
  dayRangeHeaderFormat: ({ start, end }, culture, local) =>
    local.format(start,'MMMM DD', culture) + ' - ' + local.format(end,'MMMM DD', culture)
};

export default formats;
